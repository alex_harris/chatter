//filename: chatlog.h
//name: Alex Harris
//date created: 10/20/11
//@Chatlog data Structure ported to c++ from ruby
//@a linked list of cmessages
#ifndef CHATLOG_H
#define CHATLOG_H

#include "cmessage.h"
#include <string.h>
#include <iostream>
using namespace std;

struct Node;

class Chatlog
{
  public:
    //@constructor
    //@initializes the data structures
    Chatlog();

    //@copy constructor
    //@copies contents of one log to another
    Chatlog(const Chatlog& rhslog);

    //@destructor
    //@clears out memory
    ~Chatlog();

    //@returns the length of the log
    int getLength();

    //@adds a message to the queue log
    //@accepts two arguments, contact string and text string
    void Add_Msg(const string contact,const string text);

    //@returns the message at a specified index
    //@accepts an index parameter
    Cmessage Get_Msg(int index);

    //@prints the contents of a message to desired output
    //@accepts message index and output such as cout
    ostream& PrintMessage(int index, ostream& output);

  private:
    //@the physical log in memory
    Node* mbackptr;
    int qLength;

};
#endif
