//@file: chatlog.cpp
//@name: alex harris
//@date: 10/21/11

//@the data structure for the chatlog ported to c++ from ruby

#include "chatlog.h"

struct Node
{
  Cmessage message;
  Node* next;
};

//@constructor
//@initializes data structures
Chatlog::Chatlog()
{
  mbackptr = NULL;
  qLength = 0;
}

//@copy consructor
//@copies contents of rhslog into Chatlog
Chatlog::Chatlog(const Chatlog& rhslog)
{
  mbackptr = NULL;
  qLength = 0;
  operator= (rhslog);
}

//@destructor
//@clears data structures from memory
//@releases all nodes from memory
Chatlog::~Chatlog()
{
  Node* tempptr = mbackptr;
  Node* nextptr = NULL;
  while (tempptr != NULL) {
    nextptr = tempptr -> next;
    delete tempptr;
    tempptr = nextptr;
  }
}

//@returns the length of the log
//@returns an integer
int Chatlog::getLength()
{
  return qLength;
}
//@adds a message to the log
//@accepts two arguments, the contact name string, and message text string
void Chatlog::Add_Msg(const string contact, const string text)
{
  Cmessage themessage (contact, text);
  Node* newMessage = new (nothrow) Node;
  if (newMessage != NULL) {
    newMessage -> next = NULL;
    newMessage -> message = themessage;
    if (mbackptr != NULL) {
      newMessage -> next = mbackptr;
    } mbackptr = newMessage;
  }qLength++;
}

//@returns a message
//@reurns the message at the given parameter
Cmessage Chatlog::Get_Msg(int index)
{
  if (index < qLength) {
    Node* tempptr = mbackptr;
    for (int i = index; i > 0; i--) {
      tempptr = tempptr -> next;
    } return tempptr -> message;
  }
}

//@prints the contents of a message to desired output
//@accepts message index and output such as cout
ostream& Chatlog::PrintMessage(int index, ostream& output)
{
  Node* tempptr = mbackptr;
  for (int i = index; i > 0; i--) {
    tempptr = tempptr -> next;
  } output << tempptr -> message.send_time << " - " << tempptr -> message.sender_name << endl;
  output << tempptr -> message.message_text << endl;
}

