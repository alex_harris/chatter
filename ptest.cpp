#include <pthread.h>
#include <iostream>
using namespace std;

pthread_t read_t;
pthread_t write_t;
pthread_mutex_t mutex;
pthread_mutexattr_t attr;

void *reader(void *tid);
void *writer(void *tid);

char buf[20];

int main() {
  
  long t;
  for (int i = 0; i < 20; i++) {
    buf[i] = i;
  }

  pthread_mutexattr_init(&attr);
  pthread_mutex_init(&mutex, &attr);

  cout << "Writing.." << endl;
  pthread_create(&write_t, NULL, writer, (void *)t);

  cout << "Reading.." << endl;
  pthread_create(&read_t, NULL, reader, (void *)t);

  pthread_exit(NULL);
  return 0;
}

void *reader(void *tid)
{
  pthread_mutex_lock(&mutex);
  for (int i = 0; i < 20; i++) {
    cout << buf[i] << endl;
  } cout << "Done Reading" << endl;
  pthread_mutex_unlock(&mutex);
}

void *writer(void *tid)
{
  pthread_mutex_lock(&mutex);
  for (int i = 0; i < 20; i++) {
    buf[19-i] = i;
  } cout << "Done Writing" << endl;
  pthread_mutex_unlock(&mutex);
}
