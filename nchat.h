//@file: chat.h
//general handler class for a chat
//includes socket connection, message recieve, message send, and updates screen
#ifndef CHAT_H
#define CHAT_H

#include "cmessage.h"
#include "chatlog.h"
#include <iostream>
using namespace std;

class NChat
{
  public:
    NChat();

    ~NChat();

  private:
    Chatlog conversation;
};
#endif

