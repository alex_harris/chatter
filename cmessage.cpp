//@file: cmessage.cpp
//@name: alex harris
//@date: 10/21/11

//@data structure cmessage ported to c++ from ruby

#include "cmessage.h"

//@constructor
//@creates an empty, timestampted message
Cmessage::Cmessage()
{
  sender_name = "";
  message_text = "";
  setTime();
}

//@constructor2 code
//@accepts 2 arguments, name, and text as strings
Cmessage::Cmessage(const string name, const string text)
{
  sender_name = name;
  message_text = text;
  setTime();
}

//@copy constructor
//@copies the contents of rhsmessage to cmessage
Cmessage::Cmessage(const Cmessage& rhsmessage)
{
  sender_name = rhsmessage.sender_name;
  message_text = rhsmessage.message_text;
  for (int i = 0; i < 20; i++) {
    send_time[i] = rhsmessage.send_time[i];
  }
}

//@destructor
//@removes the class data structures
Cmessage::~Cmessage()
{ 
  sender_name.clear();
  message_text.clear();
  send_time.clear();
}

//@another for mof copy constructor
Cmessage& Cmessage::operator=(const Cmessage& rhsmessage)
{
  if (this != &rhsmessage) {
    sender_name = rhsmessage.sender_name;
    message_text = rhsmessage.message_text;
    for (int i = 0; i < 20; i++) {
      send_time[i] = rhsmessage.send_time[i];
    }
  } return *this;
}

//@sets the timestamp to current time
void Cmessage::setTime()
{
  char c_time [13];
  time_t rawtime;
  struct tm * timeinfo;
  time (&rawtime);
  timeinfo = localtime(&rawtime);
  strftime (c_time, 13, "Sent at %I:%M", timeinfo);
  string string_time (c_time);
  send_time = string_time;
}

