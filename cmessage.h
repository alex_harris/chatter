//file: cmessage.h
//name: alex Harris
//date created: 10/20/11
//@header for data structure message, ported to c++ from ruby
#ifndef CMESSAGE_H
#define CMESSAGE_H

#include <iostream>
#include <string>
#include <time.h>
using namespace std;

class Cmessage
{
  public:
    string sender_name;
    string message_text;
    string send_time; 

    //@constructor
    //@creates an empty, timestamped message
    Cmessage();

    //@constructor2
    //@accepts two arguments the sender name and message text as strings
    Cmessage(const string name, const string text);

    //@copy constructor
    //@copies the contents of rhs_message to message
    Cmessage(const Cmessage& rhsmessage);
    
    //@destructor
    //@removes data structures
    ~Cmessage();

    //@another form of copy
    Cmessage& operator=(const Cmessage& rhsmessage);

    //@sets the timestamp to current
    void setTime();

};
#endif
