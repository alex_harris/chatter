nchat.o: nchat.cpp nchat.h
	g++ -c nchat.cpp

logtester: logtester.cpp chatlog.o cmessage.o
	g++ -o logtester logtester.cpp chatlog.o cmessage.o

chatlog.o: chatlog.cpp chatlog.h
	g++ -c chatlog.cpp

cmessage.o: cmessage.cpp cmessage.h
	g++ -c cmessage.cpp
        
clean:
	rm *.o

