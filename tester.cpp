#include <ncurses.h>
#include <string.h>

int main() {

  initscr();
  cbreak();
  noecho();
  nonl();
  intrflush(stdscr, FALSE);
  keypad(stdscr, TRUE);

  start_color();
  init_pair(1,COLOR_BLUE,COLOR_WHITE);
  init_pair(2,COLOR_GREEN,COLOR_WHITE);
  init_pair(3,COLOR_BLACK,COLOR_WHITE);

  wbkgd(stdscr,COLOR_PAIR(3));
  for (int i = 0; i < LINES-1; i++) {
    mvprintw(i, COLS-1, "|");
    mvprintw(i, 0, "|");
  }
  for (int i = 0; i < (3*LINES)/4; i++) {
    mvprintw(i, (3*COLS)/4-1, "|");
  }
  for (int i = 0; i < COLS; i++) {
    mvprintw((3*LINES)/4-1, i, "=");
  }
  for (int i = 0; i < COLS-1; i++) {
    mvprintw(0, i, "-");
    mvprintw(LINES-1, i, "-");
  }
  refresh();

  WINDOW *win1;
  WINDOW *win2;
  WINDOW *win3;

  win1 = newwin(((3*LINES)/4)-2, ((3*COLS)/4)-2, 1, 1);
  win2 = newwin(((3*LINES)/4)-2, (COLS-(3*COLS)/4)-2, 1, ((3*COLS)/4)+1);
  win3 = newwin((LINES-(3*LINES)/4)-1, COLS-2, ((3*LINES)/4), 1);

  wbkgd(win1,COLOR_PAIR(3));
  wbkgd(win2,COLOR_PAIR(2));
  wbkgd(win3,COLOR_PAIR(1));

  wprintw(win1, "Sent 05:40 - Alex Harris\nHi how are you doing today?");
  wprintw(win2, "Alex Harris\n");
  wprintw(win2, "Not Alex Harris\n");
  wprintw(win3, "**Enter Text Here**");

  wrefresh(win1);
  wrefresh(win2);
  wrefresh(win3);

  char ch = wgetch(win3);
  while (ch != 'o') {
    waddch(win3, ch);
    waddch(win3, int(ch));
    ch = wgetch(win3);
  }

  delwin(win1);
  delwin(win2);
  delwin(win3);
  endwin();

  return 0;
} 

